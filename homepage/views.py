from django.shortcuts import render, redirect
from .forms import ProductForm
from .models import Matkul

# Create your views here.
def story1(request):
    return render(request, 'story1.html')


def story3(request):
    return render(request, 'Story3.html')


def contacts(request):
    return render(request, 'Contacts.html')


def matkul(request):
    obj = Matkul.objects.all()
    context = {
        'object': obj
    }
    return render(request, "Matkul.html", context)


def matkulAdd(request):
    form = ProductForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('homepage:matkul')

    context = {
        'form': form
    }
    return render(request, "CreateMatkul.html", context)


# def MatkulDetail(request, my_id):
#     obj = Matkul.objects.get(id=my_id)
#     context = {
#         'object':obj
#     }
#     return render(request, "MatkulDetail.html", context)


# def DeleteView(request, my_id):
#     Matkul.objects.get(id=my_id).delete()
#     return redirect('story1')
