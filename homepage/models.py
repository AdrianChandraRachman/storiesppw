from django.db import models

# Create your models here.

class Matkul(models.Model) :
    title = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.PositiveSmallIntegerField()
    description = models.TextField()
    semester = models.PositiveIntegerField()
    room = models.CharField(max_length=10)


    def __str__(self):
        return self.title

